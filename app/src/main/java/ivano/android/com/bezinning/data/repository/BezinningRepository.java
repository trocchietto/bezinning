package ivano.android.com.bezinning.data.repository;


import java.util.List;

import io.reactivex.Flowable;
import ivano.android.com.bezinning.data.model.BezinningModel;


public interface BezinningRepository {
    Flowable<List<BezinningModel>> getBezinnings(String tags);
////http://nl.dev.lectorium.hu/api/v1.0/article?filter[section_id]=132


}
