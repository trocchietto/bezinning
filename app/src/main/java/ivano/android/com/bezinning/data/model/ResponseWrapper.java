package ivano.android.com.bezinning.data.model;

import com.google.gson.annotations.SerializedName;


public class ResponseWrapper<T> {

    @SerializedName("data")
    private T type;

    public T getType() {
        return type;
    }
}
