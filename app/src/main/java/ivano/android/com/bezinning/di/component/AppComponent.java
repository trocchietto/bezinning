package ivano.android.com.bezinning.di.component;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Component;
import ivano.android.com.bezinning.ui.bezinningList.MainFragment;
import ivano.android.com.bezinning.di.module.BezinningApiModule;
import ivano.android.com.bezinning.di.module.RepositoryModule;
import ivano.android.com.bezinning.di.module.SchedulerModule;

@Singleton
@Component(modules = {BezinningApiModule.class, RepositoryModule.class, SchedulerModule.class})

public interface AppComponent {
    Context context();

    void inject(MainFragment mainFragment);


}
