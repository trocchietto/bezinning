package ivano.android.com.bezinning.data.datasource.localdatasource

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query

import io.reactivex.Flowable


@Dao
interface BezinningDbDao {


    @Query("SELECT * FROM bezinningDb ")
    fun getAllBezinningDB(): Flowable<List<BezinningDb>>

    //this annotation just supplies the object
    @Insert
    fun insert(bezinningDb: BezinningDb)

}
