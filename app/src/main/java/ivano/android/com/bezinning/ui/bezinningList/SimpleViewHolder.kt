package ivano.android.com.bezinning.ui.bezinningList

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import ivano.android.com.bezinning.R


internal class SimpleViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    var textView: TextView

    init {
        textView = itemView.findViewById(R.id.textView) as TextView
    }


    fun bindData(bezinningModelData: String) {
        textView.text = bezinningModelData
    }
}
