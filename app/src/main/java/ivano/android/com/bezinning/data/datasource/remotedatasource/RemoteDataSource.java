package ivano.android.com.bezinning.data.datasource.remotedatasource;

import java.util.List;

import io.reactivex.Observable;
import ivano.android.com.bezinning.data.model.BezinningModel;


public interface RemoteDataSource<T> {
    Observable<List<BezinningModel>> getAllRemote(String tags);


}
