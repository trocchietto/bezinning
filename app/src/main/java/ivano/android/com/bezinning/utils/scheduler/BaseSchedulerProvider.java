package ivano.android.com.bezinning.utils.scheduler;

import io.reactivex.Scheduler;


public interface BaseSchedulerProvider {

    Scheduler io();

    Scheduler ui();

    Scheduler computation();


}
