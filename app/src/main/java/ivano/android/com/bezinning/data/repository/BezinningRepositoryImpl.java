package ivano.android.com.bezinning.data.repository;

import java.util.List;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import ivano.android.com.bezinning.data.datasource.remotedatasource.RemoteDataSource;
import ivano.android.com.bezinning.data.model.BezinningModel;

public class BezinningRepositoryImpl implements BezinningRepository {

    private RemoteDataSource<BezinningModel> bezinningRemoteDataSource;


    public BezinningRepositoryImpl(RemoteDataSource<BezinningModel> bezinningRemoteRemoteDataSource) {
        this.bezinningRemoteDataSource = bezinningRemoteRemoteDataSource;

    }

    @Override
    public Flowable<List<BezinningModel>> getBezinnings(String tags) {
        return bezinningRemoteDataSource.getAllRemote(tags).toFlowable(BackpressureStrategy.BUFFER);
    }


}
