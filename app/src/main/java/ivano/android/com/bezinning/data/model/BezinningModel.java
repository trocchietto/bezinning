package ivano.android.com.bezinning.data.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;


public class BezinningModel {

    private String id;
    private String type;
    @SerializedName("attributes")
    private Attribute attribute;


    public String getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public Attribute getAttribute() {
        return attribute;
    }
}
