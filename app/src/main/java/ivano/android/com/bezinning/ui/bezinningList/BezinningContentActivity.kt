package ivano.android.com.bezinning.ui.bezinningList

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.TextView
import ivano.android.com.bezinning.R
import android.provider.AlarmClock.EXTRA_MESSAGE
import android.text.Html



class BezinningContentActivity : AppCompatActivity() {
    //I had to mofify the constructor and super, eliminating PersistentBundle, because does not allow the Explicit Intent to be started under the hood, as seen in Layout Inspector
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.bezinning_content)
        val intent = intent
        val message = intent.getStringExtra(EXTRA_MESSAGE)
        val bodyBezinningParsedHtml = Html.fromHtml(message)
        val bezinningText = findViewById(R.id.bezinning_text) as TextView

        bezinningText.text = bodyBezinningParsedHtml

    }
}