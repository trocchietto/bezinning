package ivano.android.com.bezinning.ui.bezinningList

import android.app.Application
import android.content.Context

import ivano.android.com.bezinning.di.component.AppComponent
import ivano.android.com.bezinning.di.component.DaggerAppComponent
import ivano.android.com.bezinning.di.module.AppModule


 class BezinningApplication : Application() {

    companion object {
        var appContext: Context? = null
            private set
    }

    var appComponent: AppComponent? = null
        private set

    override fun onCreate() {
        super.onCreate()
        BezinningApplication.appContext = applicationContext
        appComponent = DaggerAppComponent.builder()
                .appModule(AppModule(this))
                .build()

    }


}

