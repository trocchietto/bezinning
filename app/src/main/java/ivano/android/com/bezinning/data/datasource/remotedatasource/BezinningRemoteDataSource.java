package ivano.android.com.bezinning.data.datasource.remotedatasource;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;
import ivano.android.com.bezinning.data.model.BezinningModel;
import ivano.android.com.bezinning.data.model.ResponseWrapper;


public class BezinningRemoteDataSource implements RemoteDataSource<BezinningModel> {

    private BezinningAPI bezinningAPI;

    public BezinningRemoteDataSource(BezinningAPI bezinningAPI) {
        this.bezinningAPI = bezinningAPI;
    }

    @Override
    public Observable<List<BezinningModel>> getAllRemote(String tags) {
        return bezinningAPI.loadBezinnings(tags)
                .map(new Function<ResponseWrapper<List<BezinningModel>>, List<BezinningModel>>() {
                    @Override
                    public List<BezinningModel> apply(@NonNull ResponseWrapper<List<BezinningModel>> listResponseWrapper) throws Exception {
                        return listResponseWrapper.getType();
                    }
                });
    }
}
