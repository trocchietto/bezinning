package ivano.android.com.bezinning.data.model


class Attribute {

    val id: Int = 0
    val self: String? = null
    var body: String? = null
    var label: String? = null
    fun getlabel(): String? {
        return label
    }

}
