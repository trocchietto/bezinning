package ivano.android.com.bezinning.ui.bezinningList

import android.content.Intent
import android.provider.AlarmClock.EXTRA_MESSAGE
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import ivano.android.com.bezinning.R
import ivano.android.com.bezinning.data.model.BezinningModel
import java.util.*


internal class BezinningAdapter : RecyclerView.Adapter<SimpleViewHolder>() {

    private var label = ArrayList<BezinningModel>()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SimpleViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.view_recyclerview_row, parent, false)
        return SimpleViewHolder(view)
    }


    override fun onBindViewHolder(holder: SimpleViewHolder, position: Int) {

        holder.bindData(label[position].attribute.label.toString())

        holder.itemView.setOnClickListener { view ->
            val item = label[position].attribute.body
            val intent = Intent(view.context, BezinningContentActivity::class.java)
            intent.putExtra(EXTRA_MESSAGE, item)
            view.context.startActivity(intent)


        }

    }


    override fun getItemCount(): Int {
        return label.size
    }

     fun setTitleData(bezinningModels: List<BezinningModel>) {
        if (bezinningModels == null) {
            return
        }
        label.addAll(bezinningModels)
        notifyDataSetChanged()


    }
}
