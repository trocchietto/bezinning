package ivano.android.com.bezinning.ui.bezinningList

import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.design.widget.Snackbar
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import io.reactivex.FlowableSubscriber
import javax.inject.Inject
import ivano.android.com.bezinning.R
import ivano.android.com.bezinning.data.model.BezinningModel
import ivano.android.com.bezinning.utils.scheduler.SchedulerProviderImpl
import org.reactivestreams.Subscription


@RequiresApi(Build.VERSION_CODES.HONEYCOMB)
class MainFragment : android.app.Fragment() {


    @Inject
    lateinit var bezinningListViewModel: BezinningLijstViewModel
    @Inject
    lateinit var schedulerProvider: SchedulerProviderImpl

    private lateinit var viewManager: RecyclerView.LayoutManager
    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: BezinningAdapter
    private lateinit var spinner:ProgressBar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (activity.application as BezinningApplication).appComponent!!.inject(this)
    }


    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater!!.inflate(R.layout.bezinning_fragment, container, false)
        spinner = view.findViewById(R.id.progressBar)as ProgressBar
        viewManager = LinearLayoutManager(activity)
        viewAdapter = BezinningAdapter()
        initRecyclerView(view)

        doNetworkCall()

        return view

    }

  private  fun initRecyclerView(view: View) {
        recyclerView = view.findViewById(R.id.recycler_view) as RecyclerView
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = viewManager
        recyclerView.adapter = viewAdapter
    }


 private fun doNetworkCall() {
   bezinningListViewModel.getBezinningLijst("132")
                ?.subscribeOn(schedulerProvider.io())
                ?.observeOn(schedulerProvider.ui())
                ?.doOnNext {
                    run {
                        spinner.visibility = View.VISIBLE
                    }
                }
                ?.retry(2)
                ?.subscribe(object : FlowableSubscriber<List<BezinningModel>> {
                    override fun onError(t: Throwable?) {
                        spinner.visibility = View.GONE
                       handleError()

                    }

                    override fun onComplete() {
                        Log.d("IVO", "onComplete: ")

                    }

                    override fun onSubscribe(s: Subscription) {
                        Log.d("IVO", "onSubscribe: ")
                        s.request(Long.MAX_VALUE)

                    }

                    override fun onNext(bezinningModels: List<BezinningModel>?) {
                        spinner.visibility = View.GONE

                        bezinningModels?.let { viewAdapter.setTitleData(it) }

                    }
                })
    }

fun handleError(){
    Snackbar.make(view.findViewById(R.id.linearLayout), getString(R.string.check_internet), Snackbar.LENGTH_INDEFINITE)
            .setAction(getString(R.string.retry)) {
                doNetworkCall()}
            .show()

}

}




