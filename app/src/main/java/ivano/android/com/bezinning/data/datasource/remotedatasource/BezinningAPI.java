package ivano.android.com.bezinning.data.datasource.remotedatasource;

import java.util.List;

import io.reactivex.Observable;
import ivano.android.com.bezinning.data.model.BezinningModel;
import ivano.android.com.bezinning.data.model.ResponseWrapper;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface BezinningAPI {
    //http://nl.dev.lectorium.hu/api/v1.0/article?filter[section_id]=132
    //@GET("/2.2/questions?order=desc&sort=creation&site=stackoverflow")
    @GET("/api/v1.0/article")
    Observable<ResponseWrapper<List<BezinningModel>>> loadBezinnings(@Query("filter[section_id]") String tags);


}


